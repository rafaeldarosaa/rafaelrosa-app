import Head from "next/head";
import Container from "../components/container";
import ProjectsList from "../components/projects/projects-list";
import TestimonialsList from "../components/testimonials/testimonials-list";
import Intro from "../components/intro";
import Layout from "../components/layout";
import Header from "../components/header";
import { getAllProjects, getAllTestimonials, getSiteInfo } from "../lib/api";

export default function Index({
  allProjects,
  allTestimonials,
  preview,
  siteInfo,
}) {
  //const heroPost = projetos[0]?.node;
  const projects = allProjects.edges;
  const testimonials = allTestimonials.edges;

  return (
    <>
      <Layout preview={preview}>
        <Head>
          <title>{siteInfo.generalSettingsTitle}</title>
        </Head>
        <Container>
          <Header />
          <Intro />
          {testimonials.length > 0 && (
            <TestimonialsList testimonials={testimonials} />
          )}
          {projects.length > 0 && <ProjectsList projects={projects} />}
        </Container>
      </Layout>
    </>
  );
}

export async function getStaticProps({ preview = false }) {
  const allProjects = await getAllProjects();
  const allTestimonials = await getAllTestimonials();
  const siteInfo = await getSiteInfo();
  return {
    props: { allProjects, allTestimonials, preview, siteInfo },
  };
}
