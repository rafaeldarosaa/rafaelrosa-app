import { useRouter } from "next/router";
import ErrorPage from "next/error";
import Container from "../../components/container";
import ProjectBody from "../../components/projects/project-body";
import ProjectsList from "../../components/projects/projects-list";
import Header from "../../components/header";
import ProjectHeader from "../../components/projects/project-header";
import SectionSeparator from "../../components/section-separator";
import Layout from "../../components/layout";
import {
  getProjectAndMoreProjects,
  getAllProjectsWithSlug,
  getSiteInfo,
} from "../../lib/api";
import ProjectTitle from "../../components/projects/project-title";
import Head from "next/head";
import Tags from "../../components/tags";

export default function Project({ project, projects, preview, siteInfo }) {
  const router = useRouter();
  const moreProjects = projects?.edges;

  if (!router.isFallback && !project?.slug) {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <Layout preview={preview}>
      <Container>
        <Header />
        {router.isFallback ? (
          <ProjectTitle>Carregando...</ProjectTitle>
        ) : (
          <>
            <article>
              <Head>
                <title>
                  {project.title} | {siteInfo.generalSettingsTitle}
                </title>
                <meta
                  property="og:image"
                  content={project.featuredImage?.sourceUrl}
                />
              </Head>
              <ProjectHeader
                title={project.title}
                coverImage={project.featuredImage}
                date={project.date}
                author={project.author}
                categories={project.categories}
              />
              <ProjectBody content={project.content} />
              <footer></footer>
            </article>

            <SectionSeparator />
            {moreProjects.length > 0 && (
              <ProjectsList projects={moreProjects} />
            )}
          </>
        )}
      </Container>
    </Layout>
  );
}

export async function getStaticProps({ params, preview = false, previewData }) {
  const data = await getProjectAndMoreProjects(
    params.slug,
    preview,
    previewData
  );
  const siteInfo = await getSiteInfo();

  return {
    props: {
      preview,
      project: data.projeto,
      projects: data.projetos,
      siteInfo: siteInfo,
    },
  };
}

export async function getStaticPaths() {
  const allProjects = await getAllProjectsWithSlug();

  return {
    paths: allProjects.edges.map(({ node }) => `/projects/${node.slug}`) || [],
    fallback: true,
  };
}
