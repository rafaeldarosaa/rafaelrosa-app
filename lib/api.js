const API_URL = process.env.WORDPRESS_API_URL;

async function fetchAPI(query, { variables } = {}) {
  const headers = { "Content-Type": "application/json" };

  if (process.env.WORDPRESS_AUTH_REFRESH_TOKEN) {
    headers[
      "Authorization"
    ] = `Bearer ${process.env.WORDPRESS_AUTH_REFRESH_TOKEN}`;
  }

  const res = await fetch(API_URL, {
    method: "POST",
    headers,
    body: JSON.stringify({
      query,
      variables,
    }),
  });

  const json = await res.json();
  if (json.errors) {
    console.error(json.errors);
    throw new Error("Failed to fetch API");
  }
  return json.data;
}

export async function getPreviewPost(id, idType = "DATABASE_ID") {
  const data = await fetchAPI(
    `
    query PreviewPost($id: ID!, $idType: PostIdType!) {
      post(id: $id, idType: $idType) {
        databaseId
        slug
        status
      }
    }`,
    {
      variables: { id, idType },
    }
  );
  return data.post;
}

export async function getAllPostsWithSlug() {
  const data = await fetchAPI(`
    {
      posts(first: 10000) {
        edges {
          node {
            slug
          }
        }
      }
    }
  `);
  return data?.posts;
}

export async function getAllPostsForHome(preview) {
  const data = await fetchAPI(
    `
    query AllPosts {
      posts(first: 20, where: { orderby: { field: DATE, order: DESC } }) {
        edges {
          node {
            title
            excerpt
            slug
            date
            featuredImage {
              node {
                sourceUrl
              }
            }
            author {
              node {
                name
                firstName
                lastName
              }
            }
          }
        }
      }
    }
  `,
    {
      variables: {
        onlyEnabled: !preview,
        preview,
      },
    }
  );

  return data?.posts;
}

export async function getPostAndMorePosts(slug, preview, previewData) {
  const postPreview = preview && previewData?.post;
  // The slug may be the id of an unpublished post
  const isId = Number.isInteger(Number(slug));
  const isSamePost = isId
    ? Number(slug) === postPreview.id
    : slug === postPreview.slug;
  const isDraft = isSamePost && postPreview?.status === "draft";
  const isRevision = isSamePost && postPreview?.status === "publish";
  const data = await fetchAPI(
    `
    fragment AuthorFields on User {
      name
      firstName
      lastName
    }
    fragment PostFields on Post {
      title
      excerpt
      slug
      date
      featuredImage {
        sourceUrl
      }
      author {
        ...AuthorFields
      }
      categories {
        edges {
          node {
            name
          }
        }
      }
      terms {
        edges {
          node {
            name
            slug
          }
        }
      }
    }
    query PostBySlug($id: ID!, $idType: PostIdType!) {
      post(id: $id, idType: $idType) {
        ...PostFields
        content
        ${
          // Only some of the fields of a revision are considered as there are some inconsistencies
          isRevision
            ? `
        revisions(first: 1, where: { orderby: { field: MODIFIED, order: ASC } }) {
          edges {
            node {
              title
              excerpt
              content
              author {
                ...AuthorFields
              }
            }
          }
        }
        `
            : ""
        }
      }
      posts(first: 3, where: { orderby: { field: DATE, order: DESC } }) {
        edges {
          node {
            ...PostFields
          }
        }
      }
    }
  `,
    {
      variables: {
        id: isDraft ? postPreview.id : slug,
        idType: isDraft ? "DATABASE_ID" : "SLUG",
      },
    }
  );

  // Draft posts may not have an slug
  if (isDraft) data.post.slug = postPreview.id;
  // Apply a revision (changes in a published post)
  if (isRevision && data.post.revisions) {
    const revision = data.post.revisions.edges[0]?.node;

    if (revision) Object.assign(data.post, revision);
    delete data.post.revisions;
  }

  // Filter out the main post
  data.posts.edges = data.posts.edges.filter(({ node }) => node.slug !== slug);
  // If there are still 3 posts, remove the last one
  if (data.posts.edges.length > 2) data.posts.edges.pop();

  return data;
}

export async function getSiteInfo() {
  const data = await fetchAPI(`
    {
      allSettings {
        generalSettingsTitle
        generalSettingsDescription
      }
    }
  `);
  return data?.allSettings;
}

export async function getAllProjects() {
  const data = await fetchAPI(
    `
    query allProjects {
      projetos {
        edges {
          node {
            slug
            title
            projeto_campos {
              pDominio
            }
            featuredImage {
              node {
                altText
                sourceUrl
              }
            }
          }
        }
      }
    }
  `
  );

  return data?.projetos;
}

export async function getProjectAndMoreProjects(slug, preview, previewData) {
  const projectPreview = preview && previewData?.projeto;
  // The slug may be the id of an unpublished post
  const isId = Number.isInteger(Number(slug));
  const isSamePost = isId
    ? Number(slug) === projectPreview.id
    : slug === projectPreview.slug;
  const isDraft = isSamePost && projectPreview?.status === "draft";
  const isRevision = isSamePost && projectPreview?.status === "publish";
  const data = await fetchAPI(
    `
    fragment ProjectFields on Projeto {
      title
      slug
      featuredImage {
        node {
          sourceUrl
        }
      }
      terms {
        edges {
          node {
            name
            slug
          }
        }
      }
    }
    query ProjectBySlug($id: ID!, $idType: ProjetoIdType!) {
      projeto(id: $id, idType: $idType) {
        ...ProjectFields
        content
        ${
          // Only some of the fields of a revision are considered as there are some inconsistencies
          isRevision
            ? `
        revisions(first: 1, where: { orderby: { field: MODIFIED, order: ASC } }) {
          edges {
            node {
              title
              content
            }
          }
        }
        `
            : ""
        }
      }
      projetos(first: 3, where: { orderby: { field: TITLE, order: DESC } }) {
        edges {
          node {
            ...ProjectFields
          }
        }
      }
    }
  `,
    {
      variables: {
        id: isDraft ? projectPreview.id : slug,
        idType: isDraft ? "DATABASE_ID" : "SLUG",
      },
    }
  );

  // Draft projects may not have an slug
  if (isDraft) data.projeto.slug = projectPreview.id;
  // Apply a revision (changes in a published post)
  if (isRevision && data.projeto.revisions) {
    const revision = data.projeto.revisions.edges[0]?.node;

    if (revision) Object.assign(data.projeto, revision);
    delete data.projeto.revisions;
  }

  // Filter out the main post
  data.projetos.edges = data.projetos.edges.filter(
    ({ node }) => node.slug !== slug
  );
  // If there are still 3 posts, remove the last one
  if (data.projetos.edges.length > 2) data.projetos.edges.pop();

  return data;
}

export async function getAllProjectsWithSlug() {
  const data = await fetchAPI(`
    {
      projetos(first: 10000) {
        edges {
          node {
            slug
          }
        }
      }
    }
  `);
  return data?.projetos;
}

// ver pq não aparece no graphql os depoimentos

export async function getAllTestimonials() {
  const data = await fetchAPI(
    `
    query allTestimonials {
      depoimentos {
        edges {
          node {
            slug
            title
            content
            depoimento_campos {
              dJob
            }
          }
        }
      }
    }
  `
  );

  return data?.depoimentos;
}
