import Link from "next/link";

export default function Header() {
  return (
    <nav className="flex items-center justify-between flex-wrap py-6">
      <div className="flex items-center flex-shrink-0 text-accent-2 mr-6">
        <img
          src="/images/rafaelrosa.jpg"
          width="42"
          height="50"
          alt="Rafael Rosa Desenvolvimento Web"
          className="neu-card no-border flat neu-rounded mr-3"
        />
        <span className="font-semibold text-xl rafael-brand tracking-wide text-accent-1">
          Rafael Rosa
        </span>
      </div>
      <div className="block flex items-center lg:hidden">
        <Link href="/portfolio">
          <a className="inline-block mt-0 lg:inline-block lg:mt-0 text-accent-2 hover:text-accent-1 mr-4 px-3 py-2 neu-card flat h-10">
            Portfolio
          </a>
        </Link>

        <button className=" px-3 py-2 text-accent-2 hover:text-accent-1 neu-card flat h-10">
          <svg
            className="fill-current h-5 w-5"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      <div className="w-full block flex-grow lg:flex justify-end lg:items-center lg:w-auto hidden">
        <div className="text-sm">
          <Link href="/portfolio">
            <a className="block mt-4 lg:inline-block lg:mt-0 text-accent-2 hover:text-accent-1 mr-4 neu-card flat h-10 px-3 py-2">
              Portfolio
            </a>
          </Link>

          <Link href="/contato">
            <a className="block mt-4 lg:inline-block lg:mt-0 text-accent-2 hover:text-accent-1 mr-4 neu-card flat h-10 px-3 py-2">
              Contato
            </a>
          </Link>
        </div>
        {/*<div>
         <a
            href="#"
            className="inline-block text-sm px-4 py-2 leading-none text-white hover:border-transparent bg-accent-1 hover:text-teal-500 hover:bg-white mt-4 lg:mt-0"
          >
            Contato
          </a>
        </div> */}
      </div>
    </nav>
  );
}
