import CoverImage from "../../components/cover-image";
import ProjectTitle from "../../components/projects/project-title";

export default function ProjetcHeader({ title, coverImage }) {
  return (
    <>
      <ProjectTitle>{title}</ProjectTitle>
      <div className="mb-8 md:mb-16 -mx-5 sm:mx-0">
        <CoverImage title={title} coverImage={coverImage} />
      </div>
    </>
  );
}
