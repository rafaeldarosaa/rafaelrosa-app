import ProjectPreview from "./project-preview";
import InfiniteSlider from "../slider";

export default function ProjectsList({ projects }) {
  return (
    <section>
      <h2 className="mb-4 text-1xl md:text-2xl font-bold tracking-tighter leading-tight">
        Conheça um pouco do meu trabalho
      </h2>
      <div className="overflow-hidden py-4">
        <InfiniteSlider items={projects} width={300} visible={3}>
          {({ node }, i) => (
            <ProjectPreview
              key={node.slug}
              title={node.title}
              slug={node.slug}
              coverImage={node.featuredImage.node}
            />
          )}
        </InfiniteSlider>
      </div>
    </section>
  );
}
