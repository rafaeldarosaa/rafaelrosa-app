import Link from "next/link";
import CoverImage from "../cover-image";
import NeuButton from "../ui/neu-button";

export default function ProjectPreview({ title, slug, coverImage }) {
  const ref = React.createRef();

  return (
    <div className="neu-card flat p-4 mr-8 bg-accent-1 select-none">
      <div className="mb-5 pointer-events-none">
        <CoverImage title={title} coverImage={coverImage} slug={slug} />
      </div>
      <h3 className="text-xl mb-3 leading-snug pointer-events-none text-center">
        <a
          className="hover:underline"
          dangerouslySetInnerHTML={{ __html: title }}
        ></a>
      </h3>

      <Link as={`/projetos/${slug}`} href="/projetos/[slug]">
        <NeuButton btnText={"ver mais"} ref={ref} />
      </Link>
    </div>
  );
}
