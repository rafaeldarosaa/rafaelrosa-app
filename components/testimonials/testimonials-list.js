import TestimonialPreview from "./testimonial-preview";
import InfiniteSlider from "../slider";

export default function TestimonialsList({ testimonials }) {
  return (
    <section>
      <h2 className="mb-4 text-1xl md:text-2xl text-center font-bold tracking-normal leading-tight mb-10 text-accent-2">
        Alguns relatos de parceiros sobre meu trabalho
      </h2>
      <div className="overflow-hidden py-4">
        <InfiniteSlider items={testimonials} width={360} visible={3}>
          {({ node }, i) => (
            <TestimonialPreview
              key={node.slug}
              title={node.title}
              content={node.content}
              job={node.depoimento_campos.dJob}
            />
          )}
        </InfiniteSlider>
      </div>
    </section>
  );
}
