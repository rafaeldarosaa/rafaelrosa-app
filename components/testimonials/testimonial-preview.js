import Link from "next/link";
import CoverImage from "../cover-image";
import NeuButton from "../ui/neu-button";

export default function TestimonialPreview({ title, content }) {
  return (
    <div className="neu-card flat p-8 mr-8 bg-accent-1 select-none">
      <div className="" dangerouslySetInnerHTML={{ __html: content }}></div>
      <h3 className="text-xl mb-3 leading-snug pointer-events-none text-left mt-4">
        <b>{title}</b>
      </h3>
    </div>
  );
}
