import { CMS_NAME, CMS_URL } from "../lib/constants";

export default function Intro() {
  return (
    <section className="flex md:flex-row items-center mt-8 mb-8 md:mb-32 md:mt-20 justify-between w-full">
      <div className="flex flex-col items-start justify-start">
        <h1 className="text-2xl md:text-5xl font-bold tracking-tight leading-loose md:pr-2 max-w-5xl text-accent-2">
          {/* Você ou seu negócio, precisa de um website? */}
          Frase que fale sobre benefícios
        </h1>
        <h2 className="text-xl md:text-xl font-normal tracking-tight leading-normal md:pr-2 max-w-2xl text-accent-2">
          Incrível, profissional, atrativo, que você mesmo seja capaz de
          gerenciar?
        </h2>
      </div>

      <img
        src="images/rafael-600x600.jpg"
        width="200"
        height="200"
        className="neu-card flat radius-full rounded-full object-cover w-24 md:w-48"
      />
    </section>
  );
}
