import Container from "./container";
import { EXAMPLE_PATH } from "../lib/constants";

export default function Footer() {
  return (
    <footer className="bg-accent-1 neu-card flat">
      <Container>
        <div className="py-28 flex flex-col lg:flex-row items-center"></div>
      </Container>
    </footer>
  );
}
